require 'spec_helper'

RSpec.describe Locaweb::TwitterClient, :vcr do
  describe '#tweets' do
    it { expect(subject.tweets).to be_a(Array) }
    it { expect(subject.tweets.count).to eq(51) }
  end

  describe '#mentions' do
    it { expect(subject.mentions).to be_a(Array) }
    it { expect(subject.mentions.count).to eq(23) }
  end

  describe '#replies' do
    it { expect(subject.replies).to be_a(Array) }
    it { expect(subject.replies.count).to eq(0) } # :(
  end

  describe '#mentions_and_replies' do
    it { expect(subject.mentions_and_replies).to be_a(Array) }
    it { expect(subject.mentions_and_replies.count).to eq(23) }
  end

  describe '#most_relevants' do
    it { expect(subject.most_relevants).to be_a(Array) }
    it { expect(subject.most_relevants.count).to eq(23) }

    it { expect(subject.most_relevants[0].user.followers_count).to eq(829) }
    it { expect(subject.most_relevants[0].retweet_count).to eq(0) }
    it { expect(subject.most_relevants[0].favorite_count).to eq(352) }

    it { expect(subject.most_relevants[1].user.followers_count).to eq(855) }
    it { expect(subject.most_relevants[1].retweet_count).to eq(8) }
    it { expect(subject.most_relevants[1].favorite_count).to eq(241) }

    it { expect(subject.most_relevants[22].user.followers_count).to eq(14) }
    it { expect(subject.most_relevants[22].retweet_count).to eq(4) }
    it { expect(subject.most_relevants[22].favorite_count).to eq(0) }
  end

  describe '#most_mentions' do
    it { expect(subject.most_mentions).to be_a(Array) }
    it { expect(subject.most_mentions.count).to eq(23) }

    it {
      expect(subject.most_mentions.first[:user]).to be_a(OpenStruct)
      expect(subject.most_mentions.first[:user][:id]).to eq(65828)
      expect(subject.most_mentions.first[:user][:screen_name]).to eq('efrain_weimann')
    }

    it {
      expect(subject.most_mentions.first[:tweets]).to be_a(Array)
      expect(subject.most_mentions.first[:tweets].count).to eq(1)
      expect(subject.most_mentions.first[:tweets].first).to be_a(OpenStruct)
      expect(subject.most_mentions.first[:tweets].first[:text]).to \
        eq('The AGP system is down, quantify the open-sourc @locaweb ')
    }
  end
end
