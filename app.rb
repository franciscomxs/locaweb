require 'sinatra'
require 'json'

if development?
  require 'sinatra/reloader'
  require 'dotenv/load'
end

require './lib/locaweb'

class App < Sinatra::Base
  set :client, Locaweb::TwitterClient.new

  get '/most_relevants' do
    content_type :json, charset: 'utf-8'
    settings.client.most_relevants.map(&:as_json).to_json
  end

  get '/most_mentions' do
    content_type :json, charset: 'utf-8'
    settings.client.most_mentions.map do |hash|
      {
        screen_name: hash[:user].screen_name,
        tweets: hash[:tweets].map(&:as_json)
      }
    end.to_json
  end
end
