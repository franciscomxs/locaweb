# Locaweb

Hey! Lets use this app to list twitter mentions for locaweb.

## Installation

Open the project:

```ruby
cd locaweb
cp .env.example .env
foreman start
```

There are two available endpoints:

### Most mentions

http://localhost:3000/most_mentions

### Most relevant

http://localhost:3000/most_relevant

## Testing

```ruby
cd locaweb
cp .env.example .env
rspec
```
