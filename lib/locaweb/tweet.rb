module Locaweb
  require 'ostruct'
  class Tweet < OpenStruct

    def as_json
      {
        screen_name: user.screen_name,
        followers_count: user.followers_count,
        retweet_count: retweet_count,
        favorite_count: favorite_count,
        text: text,
        created_at: created_at
      }
    end
  end
end
