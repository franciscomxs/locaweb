module Locaweb
  class TwitterClient
    require 'faraday'
    require 'json'

    LOCAWEB_ID = 42
    LOCAWEB_SCREEN_NAME = 'locaweb'

    def tweets
      @tweets ||= get('/mentions_timeline').statuses
    end

    def mentions
      @mentions ||= tweets.select do |tweet|
        tweet.entities.user_mentions.any? &&
        tweet.entities.user_mentions.map(&:id).include?(LOCAWEB_ID)
      end
    end

    def replies
      @replies ||= tweets.select do |tweet|
        mention_ids.include?(tweet.in_reply_to_status_id)
      end
    end

    def mentions_and_replies
      mentions.concat(replies)
    end

    def most_relevants
      @most_relevants ||= mentions_and_replies.sort do |a, b|
        tweet_weight(b) <=> tweet_weight(a)
      end
    end

    def most_mentions
      mentions_and_replies.map(&:user).sort do |a, b|
        user_weight(b) <=> user_weight(a)
      end.map do |user|
        {
          user: user,
          tweets: user_tweets(user)
        }
      end
    end

    private

    def client
      @client ||= Faraday.new(
        url: 'http://tweeps.locaweb.com.br/tweeps',
        headers: headers
      )
    end

    def get(url, params = {})
      JSON.parse(client.get(url, params).body,
        symbolize_names: true,
        object_class: Locaweb::Tweet)
    end

    def headers
      { 'Username' => ENV['USERNAME'] }
    end

    def mention_ids
      mentions.map(&:id)
    end

    def tweet_weight(tweet)
      [
        tweet.user.followers_count,
        tweet.retweet_count,
        tweet.favorite_count
      ].inject(&:+)
    end

    def user_weight(user)
      mentions_and_replies.map do |tweet|
        tweet.user.id
      end.count(user.id)
    end

    def user_tweets(user)
      mentions_and_replies.select do |tweet|
        tweet.user.id == user.id
      end
    end
  end
end
